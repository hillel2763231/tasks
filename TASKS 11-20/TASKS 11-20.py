#                                       TASK 11
# Дано двоцифрове число. Знайдіть число десятків у ньому.

number_number = int(input("Введіть двоцифрове додатнє число: "))
decima = number_number // 10
print("Десятків у числі:", decima)


#                                       TASK 12
# Напишіть програму для друку чисел, у яких розділювачами груп розрядів (групи по три цифри) є коми.

number = int(input("Введіть число: "))
formatted_number = "{:,}".format(number)
print(formatted_number)


#                                       TASK 13
# Напишіть програму, яка отримує значення радіуса кола (дробове число) від користувача та обчислює площу круга і довжину колу.
# Виведення результату відбувається з трьома символами після десяткової крапки. Значення числа Пі округлити до сотих.

radius = input("Значення радіусу кола: ")

rad_float = float(radius)

square = 3.14 * (rad_float * rad_float)
length = 2 * (3.14 * rad_float)

print('Площа круга -->'  '%.3f' % square)
print('Довжина круга -->'  '%.3f' % length)


#                                       TASK 14
# Напишіть програму для запису і розв’язування виразу (a + b) * (a + b), де a і b - натуральні цілі числа.

a = input("Значення а: ")
b = input("Значення b: ")

if a.isdigit() and b.isdigit():
    a_int = int(a)
    b_int = int(b)
    print(f'{(a_int + b_int) * (a_int + b_int)}')


#                                       TASK 15
# Напишіть програму для перетворення висоти (вказується окремо у футах (1 фут = 30,48 см) і дюймах (1 дюйм = 2,54 см)) у сантиметри.

feet = input('Enter feets to convert in centimetre: ')
inch = input('Enter inch to convert in centimetre: ')

if feet.isdigit() and inch.isdigit():
    feet_cm = int(feet) * 30.48
    inch_cm = int(inch) * 2.54

    print(f'Your feets and inches = {feet_cm + inch_cm} cm')


#                                       TASK 16
# Напишіть програму для перетворення відстані (вказана у футах)
# у дюйми (1 фут = 12 дюймів), ярди (1 фут = 0,333333333 ярда) і милі (1 фут = 0,000189393939 милі).

feet_inpt = input('Enter feet meaning: ')

if feet_inpt.isdigit():
    inches = int(feet_inpt) * 12
    yards = int(feet_inpt) *  0.333333333
    miles = int(feet_inpt) * 0.000189393939

    print('The distance in inches is ' '%.0f' % inches, 'inches.')
    print('The distance in yards is ' '%.3f' % yards, 'yards.')
    print('The distance in miles is ' '%.3f' % miles, 'miles.')


#                                       TASK 17
# Напишіть програму, щоб отримати ASCII значення введеного з клавіатури символа.

symbol = input("Введіть символ: ")

ascii_value = ord(symbol)
print(f"ASCII значення символа '{symbol}' - {ascii_value}")


#                                       TASK 18
# Напишіть програму, щоб вивести символ за введеним ASCII значенням.

ascii_value = int(input("Введіть ASCII значення: "))

symbol = chr(ascii_value)
print(f"Символ за ASCII значенням {ascii_value} - '{symbol}'")


#                                       TASK 19
# Користувач вводить значення чисел a і b з клавіатури.
# Напишіть програму для друку дії додавання і результату обчислення як у вихідних даних.

a_input = input('Input a: ')
b_input = input('Input b: ')

if a_input.isdigit() and b_input.isdigit():
    a_inp = int(a_input)
    b_inp = int(b_input)
    print(f'{a_input} + {b_input} = {a_inp + b_inp}')


#                                       TASK 20
# Напишіть програму, яка зчитує значення двох змінних a і b, потім змінює їх значення місцями
# (тобто в змінній a має бути записано те, що раніше зберігалося в b, а в змінній b записано те, що раніше зберігалося в a).
# Потім виведіть значення змінних. Виконайте подане завдання без використання третьої змінної.
# У якому порядку значення змінних були введені, у тому ж порядку повинні і виводитися.

a2 = input('Input a: ')
b2 = input('Input b: ')

if a2.isdigit() and b2.isdigit():
    a, b = b, a
    print(f'Значення після обміну: a = {a2}, b = {b2}')
