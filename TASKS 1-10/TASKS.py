#                                       TASK 1
# Напишіть програму, яка отримує три цілих числа, введені з клавіатури (кожне число вводиться на окремому рядку),
# і друкує на екрані їх суму, добуток, результат піднесення першого числа до степеня різниці другого і третього чисел.

num1 = input('First number: ')
num2 = input('Second number: ')
num3 = input('Third number: ')

if num1.isdigit() and num2.isdigit() and num3.isdigit():
    num1 = int(num1)
    num2 = int(num2)
    num3 = int(num3)

    print(f'{num1 + num2 + num3}')
    print(f'{num1 * num2 * num3}')
    print(f'{num1 ** num2 - num3}')

else:
    print(f'ONLY NUMBERS!')



#                                       TASK 2
# Напишіть програму, яка приймає ціле число n і обчислює значення виразу n + nn + nnn.

n = input('Your number: ')

if n.isdigit(): # Цією дією я переконався що воно є саме числом
    n = str(n)
    print(f'{n} + {n}{n} + {n}{n}{n}')

else:
    print(f'ONLY NUMBERS!')



#                                       TASK 3
# Напишіть програму, яка отримує такі дані: ім’я, вік, хобі, введені з клавіатури (вводяться на окремих рядках),
# і друкує на екрані одним повідомленням повну інформацію на основі введених даних.

name = input('Name: ')
age = input('Age: ')
hobby = input('Hobby: ')

if age.isalpha():
    print(f'IN AGE - ACCEPTABLE NUMBERS!')

elif hobby.isdigit():
    print(f'WRITE HOBBY BY LETTERS!')

print(f'My name is {name} . I am {age} and my hobby is {hobby}.')



#                                       TASK 4
# Напишіть програму, яка зчитує довжину основи та висоту прямокутного трикутника (цілі числа),
# обчислює площу і друкує її значення на екрані у відформатованому вигляді (два символи після десяткової крапки).
# Кожен параметр вводиться на окремому рядку.

length = input('Enter length: ')
high = input('Enter high: ')

if length.isalpha() and high.isalpha():
    print(f'ACCEPTABLE ONLY NUMBERS!')

i = float(0.5 * (int(length) * int(high)))
print('%.2f' % i)


#                                       TASK 5
# Напишіть програму, яка зчитує ціле число, і друкує попереднє та наступне числа відносно введеного.

number = input('Enter your number: ')

if number.isalpha() :
    print(f'ACCEPTABLE ONLY NUMBERS!')

elif number.isdigit():
    number = int(number)

print(f'The next number for the number {number} is {number + 1}.'
      f'The previous number for the number {number} is {number - 1}.')


#                                       TASK 6
# Вводиться ціле додатне число. Надрукуйте останню цифру числа.

nnn = int(input("Введіть ціле додатнє число: "))
last_digit = nnn % 10
print("Остання цифра числа:", last_digit)
#??????????????????????????????????????????????????????????????????????????????????????????????????????

nr = int(input("Введіть ціле додатнє число: "))
number_str = str(nr)
last_d = number_str[-1]
print("Остання цифра числа: ", last_d)


#                                       TASK 7
# Напишіть програму, яка друкує на екрані наступне повідомлення: Hello, Starlink subscriber!
# Your current balance is 125.56 UAH.
# Назва стільникової мережі і значення балансу вводиться з клавіатури.

network = input('Enter your network: ')
balance = input('Enter your balance: ')

if balance.isalpha() :
    print(f'ACCEPTABLE ONLY NUMBERS!')


elif balance.isdigit():
    balance = float(balance)

    print(f'Hello, {network} subscriber! Your current balance is {balance} UAH')

#???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????

#                                       TASK 8
# Напишіть програму, яка приймає ім’я та прізвище від користувача у різних рядках
# та роздруковує їх у зворотному порядку з пропуском між ними у вітальному повідомленні.

named = input('Enter your name: ')
surname = input('Enter your surname: ')

named = named.strip()
surname = surname.strip()


print(f'Hello, {surname} {named}!')


#                                       TASK 9
# Напишіть програму, яка вітає користувача, виводячи слово Hello, введене ім’я і розділові знаки за зразком (дивитися приклади вхідних і вихідних даних).
# Програма повинна зчитувати в рядкову змінну значення і виводити відповідне вітання.
# Зверніть увагу, що після коми повинен обов’язково стояти пропуск, а перед знаком оклику пропуску немає.
# Операцією конкатенації (об’єднанням) рядків (+) користуватися не можна.

name1 = input('What`s your name? Write: ')
print(f'Hello, {name1}!')


#                                       TASK 10
# Вводиться ціле невід’ємне число n (n ≤ 100). Виведіть 2 в ступені n.

nmbr = input('Number: ')

if nmbr.isalpha() :
    print(f'ACCEPTABLE ONLY NUMBERS!')

elif nmbr.isdigit() :
    nmbr = int(nmbr)
    if nmbr <= 100:
        print(f'{2 ** nmbr}')


